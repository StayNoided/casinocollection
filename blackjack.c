/*
 * 21.c // Un projet Open Source du Blackjack sur C.
 *
 *  Crée: 10 fév 2020
 *  Fini: 12 fèv 2020
 *      Auteur: StayNoided // https://gitlab.com/StayNoided/421.c
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <windows.h>

void Color(int couleurDuTexte,int couleurDeFond) // fonction d'affichage de couleurs
{
        HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
        SetConsoleTextAttribute(H,couleurDeFond*16+couleurDuTexte);
        //Utiliser Color(1,1);
}
int cardDraw(int _iMin, int _iMax){
        /* Fait un chiffre au hasard pour lancer les dés */
	    return (_iMin + (rand () % (_iMax-_iMin+1)));
}
void sleep(unsigned long int n) {
        /* boucle vide parcourue (n * 100000) fois*/
        int i = 0;
        unsigned long int max = n * 100000;
        do {
                /* Faire qqch de stupide qui prend du temps */
                i++;
        }
        while(i <= max);
}

void Card(int diceValue){
    /* Affiche les dés */
	 switch(diceValue)
	    {
	        case 1 :
	        printf(" .------. \n");
	        printf(" |1.--. | \n");
	        printf(" | :(): | \n");
	        printf(" | (__) | \n");
	        printf(" | '--'1| \n");
	        printf(" `------' \n");
	        break;
	        case 2 :
	        printf(" .------. \n");
	        printf(" |2.--. | \n");
	        printf(" | (  ) | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'2| \n");
	        printf(" `------' \n");
	        break;
	        case 3 :
	        printf(" .------. \n");
	        printf(" |3.--. | \n");
	        printf(" | :(): | \n");
	        printf(" | ()() | \n");
	        printf(" | '--'3| \n");
	        printf(" `------' \n");
	        break;
	        case 4 :
	        printf(" .------. \n");
	        printf(" |4.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'4| \n");
	        printf(" `------' \n");
	        break;
	        case 5 :
	        printf(" .------. \n");
	        printf(" |5.--. | \n");
	        printf(" | :(): | \n");
	        printf(" | (__) | \n");
	        printf(" | '--'5| \n");
	        printf(" `------' \n");
	        break;
	        case 6 :
	        printf(" .------. \n");
	        printf(" |6.--. | \n");
	        printf(" | (  ) | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'6| \n");
	        printf(" `------' \n");
	        break;
	        case 7 :
	        printf(" .------. \n");
	        printf(" |7.--. | \n");
	        printf(" | :(): | \n");
	        printf(" | ()() | \n");
	        printf(" | '--'7| \n");
	        printf(" `------' \n");
	        break;
	        case 8 :
	        printf(" .------. \n");
	        printf(" |8.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'8| \n");
	        printf(" `------' \n");
	        break;
	        case 9 :
	        printf(" .------. \n");
	        printf(" |9.--. | \n");
	        printf(" | :(): | \n");
	        printf(" | (__) | \n");
	        printf(" | '--'9| \n");
	        printf(" `------' \n");
	        break;
	        case 10 :
	        printf(" .------. \n");
	        printf(" |10--. | \n");
	        printf(" | (  ) | \n");
	        printf(" | :  : | \n");
	        printf(" | '--10| \n");
	        printf(" `------' \n");
	        break;
	        case 11 :
	        printf(" .------. \n");
	        printf(" |V.--. | \n");
	        printf(" | :(): | \n");
	        printf(" | ()() | \n");
	        printf(" | '--'V| \n");
	        printf(" `------' \n");
	        break;
	        case 12 :
	        printf(" .------. \n");
	        printf(" |D.--. | \n");
	        printf(" | :(): | \n");
	        printf(" | ()() | \n");
	        printf(" | '--'D| \n");
	        printf(" `------' \n");
	        break;
	        case 13 :
	        printf(" .------. \n");
	        printf(" |R.--. | \n");
	        printf(" | (  ) | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'R| \n");
	        printf(" `------' \n");
	        break;
	        case 14 :
	        printf(" .------. \n");
	        printf(" |A.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'A| \n");
	        printf(" `------' \n");
	        break;


	    }

}
int crtn(int cardPd){
    /* Affecte les cartons aux exceptions(2-8 cartons) et aux autres combinaisons(1 carton) */
	int Cartons;
	 switch(cardPd)
	    {
	        case 1 :
	        	Cartons=1;
	        break;
	        case 2:
	        	Cartons=2;
	        break;
	        case 3:
	        	Cartons=3;
	        break;
	        case 4 :
	        	Cartons=4;
	        break;
	        case 5 :
	        	Cartons=5;
	        break;
	        case 6 :
	            Cartons=6;
	        break;
	        case 7 :
	            Cartons=7;
	        break;
	        case 8 :
	            Cartons=8;
	        break;
	        case 9 :
	            Cartons=9;
	        break;
	        case 10 :
                Cartons=10;
	        break;
	        case 11 :
	        	Cartons=10;
	        break;
	        case 12 :
	            Cartons=10;
	        break;
	        case 13 :
	            Cartons=10;
	        break;
	        case 14 :
	            Cartons=11;
	        break;

	    }
	 return Cartons;
	 /* Ceci attribue le nb de Cartons
	  *
	  */
}

void main()
{
    int cartonsP, cartonsO, nbCartons, menuChoice, waitCycle, onze;
	int dP1, dP2, dP3, dP4, dP5, dP6, scoreP ,ValeurP, totalP; /* ints Joueurs */
	int dO1, dO2, dO3, dO4, dO5, dO6, ValeurO, scoreO, totalO; /* Ordis */
	int parties, cont, Valeur, pick,nbCartes;
	char nbManches;
    int stime;
    Color(8,0);
    printf("Bienvenue au:\n");
    printf(" ____  _ \n");
    printf("|___ ./ |\n");
    printf("  __) | |\n");
    printf(" / __/| |\n");
    printf("|_____|_|.c\n");
    printf("Edition Solitude Maxi\n\n");
    printf("Ceci est un projet scolaire de demonstration qui est disponible\nen open-source sur https://gitlab.com/StayNoided/21.c\n\n\n");
    Color(15,0);
    printf("\nAppuyez sur [ENTREE] pour demarrer la partie\n\n\n\n");
    waitCycle=0;
    getch();
    scoreO = 0;
    scoreP = 0;

    parties=0;

    cont=1;
    while (cont<=1&&cont>-1){ /* verifie si continue ou pas */
    pick=1;
    printf("Distribution des cartes: [ENTREE] pour voir les cartes\n");
    totalO = 0;
    totalP = 0;
    srand(time(NULL));
    dP1 = cardDraw(1,14);
    dP2 = cardDraw(1,14);
    dO1 = cardDraw(1,14);
    dO2 = cardDraw(1,14);
    totalP = crtn(dP1) + crtn(dP2);
    totalO = crtn(dO1) + crtn(dO2);
    getch();
    Color(15,2);
    Card(dP1);
    Card(dP2);
    Color(15,0);
    nbCartes=2;
    while (pick==1){ /* verifie si continue ou pas */

    if (nbCartes==3){
        dP3=cardDraw(1,14);
        dO3=cardDraw(1,14);
        totalP=totalP+dP3;
        totalO=totalO+dO3;
        Color(15,2);
        Card(dP1);
        Card(dP2);
        Card(dP3);
        Color(15,0);


    }
    if (nbCartes==4){
        dP4=cardDraw(1,14);
        dO4=cardDraw(1,14);
        totalP=totalP+dP4;
        totalO=totalO+dO4;
        Color(15,2);
        Card(dP1);
        Card(dP2);
        Card(dP3);
        Card(dP4);
        Color(15,0);

    }
    if (nbCartes==5){
        dP5=cardDraw(1,14);
        dO5=cardDraw(1,14);
        totalP=totalP+dP5;
        totalO=totalO+dO5;
        Color(15,2);
        Card(dP1);
        Card(dP2);
        Card(dP3);
        Card(dP4);
        Card(dP5);
        Color(15,0);

    }
    if (nbCartes==6){
    nbCartes++;
        dP6=cardDraw(1,14);
        dO6=cardDraw(1,14);
        totalP=totalP+dP6;
        totalO=totalO+dO6;
        Color(15,2);
        Card(dP1);
        Card(dP2);
        Card(dP3);
        Card(dP4);
        Card(dP5);
        Card(dP6);
        Color(15,0);

    }
    printf("\n\nVous avez un total de %d\n",totalP);
    if(totalP>=21 || totalO>=21){
    printf("\n\nVous avez un total de %d, vous ne piocherez pas.\n",totalP);
    if(totalP>21){
    Color(12,0);
    printf("\n\nVous avez cramé\n",totalP);
    Color(15,0);

    totalP=0;
    }
    if(totalO==21){
     printf("\n\nL'Ordinateur a fait un Blackjack\n");
    }
    if(totalO>21){
    Color(10,0);
    printf("\n\nVous avez gagne avec (%d), l'ordinateur a cramé (%d)\n\n",totalP,totalO);
    Color(15,0);
    totalO=0;
    }


    pick = 0;
    }


    else{
        nbCartes++;
        printf("\nVoulez vous piocher une carte?");
        printf("\n\nVous avez (%d), mais grace a la triche vous savez que l'ordinateur a (%d)\nEntrez '0' pour NE PAS piocher\n",totalP,totalO);
        scanf("%d",&pick);


    }
    }
    printf("Attribution des scores: [ENTREE] pour continuer\n");
    getch();


    if (totalP>totalO){
        scoreP++;
        printf("+1 point pour le joueur\n\n");

    }
    else if (totalP<totalO){
        scoreO++;

        printf("\n+1 point pour l'ordi\n\n");

    }


    printf("\nVous avez un total de %d point",scoreP);
    if (scoreP<=1){
        printf("\n\n");
    }
        else{
        printf("s\n\n");
    }

    printf("L'ordi a un total de %d point",scoreO);
    if (scoreO<=1){
        printf("\n\n");
    }
        else{
        printf("s\n\n");
    }




    printf("\n\nContinuer?\n\n Oui <0>, Oui & [REMISE A ZERO] <1> ou Non <2>\n");
    scanf("%d",&cont);
    if(cont<0||cont>2){
    while(cont<0||cont>2){
    printf("\n\nRequete invalide, veuillez repondre par oui ou par non.\n\n");
    printf("Continuer?\n\n Oui <0>, Oui & [REMISE A ZERO] <1> ou Non <2>");
    scanf("%d",&cont);
    }
    }
    if(cont==1){
        totalP=0;
        totalO=0;
        cartonsP=0;
        scoreO=0;
        scoreP=0;
        cartonsO=0;

    }
    printf("\n\n\n");


    }

    /* Scoreboard de fin. */
    printf("\nJoueur: %d point \n",scoreP);
    printf("\nOrdi: %d point \n",scoreO);
    if (scoreP<scoreO){
        printf("\nL'ORDI a gagne avec %d points \n\n",scoreO);
    }
    else if (scoreP>scoreO){

        printf("\nLe joueur a gagne avec &d points \n\n",scoreP);
    } else if (scoreP=scoreO){
    printf("\nEgalite: Vous et l'ORDI avez %d pats \n\n",scoreO);
    }



}
